FROM php:8.2-cli
RUN docker-php-ext-install pdo pdo_mysql && docker-php-ext-configure pcntl --enable-pcntl  && docker-php-ext-install  pcntl
WORKDIR /app