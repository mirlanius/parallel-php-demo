<?php

/*
  создание фейковых записей в базе данных для проведения демонстрации работы приложения
*/

//количество строк для вставки
$rows_to_insert = 500;

date_default_timezone_set('UTC');
$host = 'database';
$db   = 'app';
$user = 'root';
$pass = 'secret';
$charset = 'utf8';

//имена для генерации почты
$random_names = ["Aurora", "Caleb", "Delilah", "Ethan", "Freya", "Gabriel", "Harper", "Isla", "Jackson", "Kira", "Leo", "Luna", "Mason", "Nova", "Olivia", "Owen", "Penelope", "Quinn", "Riley", "Sawyer", "Scarlett", "Sebastian", "Sophie", "Theodore", "Violet", "William", "Xander", "Yasmin", "Zachary", "Zoey"];

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
$pdo = new PDO($dsn, $user, $pass, $opt);

// коннект к базе данных
// используется составной индекс, согласно запросу из рассылки
$stn = $pdo->prepare("CREATE TABLE IF NOT EXISTS mailing_list(
    `username` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255) NOT NULL,
    `validts` INT NOT NULL,  
    `confirmed` BIT(1) DEFAULT 0,
    `valid` BIT(1) DEFAULT 0,
    `last_sended` INT DEFAULT 0,
    PRIMARY KEY(email),
    INDEX params(confirmed, valid, last_sended, validts)
)", []);

$stn->execute();

//clear database if exist
$stn = $pdo->prepare("TRUNCATE TABLE mailing_list");
$stn->execute();

// вставка демо данных
for($i = 0; $i < $rows_to_insert;  $i++) {
    $username = $random_names[rand(0, count($random_names)-1)];
    srand($i);
    $email = $username.rand().rand()."@mail.com";
    $validts = rand(strtotime('now'), strtotime('now - 4 days'));
    $confirmed = rand(0,10) > 2; //увеличиваем процент выпадания true
    $valid =  rand(0,10) > 2;      

    $stn = $pdo->prepare("
        INSERT INTO mailing_list
        (username, email, validts, confirmed, valid) VALUES
        (:username$i, :email$i, :validts$i, :confirmed$i, :valid$i) ");
        
    $stn->bindValue(":username$i", $username);
    $stn->bindValue(":email$i", $email);
    $stn->bindValue(":validts$i", $validts, PDO::PARAM_INT);
    $stn->bindValue(":confirmed$i", $confirmed, PDO::PARAM_INT);
    $stn->bindValue(":valid$i", $valid, PDO::PARAM_INT);
    $stn->execute();
    echo round(100 / $rows_to_insert *$i), "% complete", "\r";
}

echo "$rows_to_insert demo users has created", PHP_EOL;