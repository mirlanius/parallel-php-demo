<?php
/**
 * отслеживание изменений в базе данных
 */

$time_interval = 5;

while(true) {    
    // создаем форк процесса для выполнения основной лигики рассылки писем
    $pid = pcntl_fork();
    if ($pid == -1) {
        die("Could not fork");
    } else if ($pid) {
        // в родительском процессе ждем заверешения форка и посылаем сигнал на удаление
        pcntl_waitpid($pid, $status);
        posix_kill($pid, SIGKILL);
    } else {
        // запуск дочернего процесса для исполнения логики рассылки
        pcntl_exec("/usr/local/bin/php", ["/app/send.php"]);
        exit(0);
    }
    // ожидание заданного интервала для паузы между проверками
    sleep($time_interval);
}