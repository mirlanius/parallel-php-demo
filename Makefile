up:
	docker-compose up -d --build

restart:
	docker-compose down && docker-compose up -d

setup:
	docker-compose exec php php setup.php

run:
	docker-compose exec php php index.php
