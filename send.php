<?php

/**
 * рассылка почты по базе данных
 */

 // количество потоков для паралельной рассылки
$threads = 10;

echo "numbers of threads: $threads", PHP_EOL;
echo "start sending...", PHP_EOL;

// запись начала отсчета времени
$start = microtime(true);
$have_mails = true;
// цикл сбора почты для обаботки
while ($have_mails) {
    $have_mails = send_batch_mails();
}

$totla_time = (int)(microtime(true) - $start);
echo "total time: $totla_time sec", PHP_EOL;
// завершение основного скрипта
die();

// функция для паралельной рассылки писем 
function send_batch_mails():bool {
    global $threads;

    $pdo = get_pdo();
    // выставляем уровень изоляции на чтение, для того чтобы гарантировать получение свежих данных из базы
    $pdo->query("SET TRANSACTION ISOLATION LEVEL READ COMMITTED");
    // запрос записей которые еще не были обработаны скриптом (ограничение по количеству потоков)
    // либо за 1 день до даты либо за 3
    $stm = $pdo->query(
        "SELECT email, validts FROM mailing_list WHERE
        confirmed = 1
        AND valid = 1
        AND  last_sended = 0
        AND (FROM_UNIXTIME(validts, '%Y-%m-%d') = DATE_SUB(CURDATE(), INTERVAL 1 DAY)
        OR 
        FROM_UNIXTIME(validts, '%Y-%m-%d') = DATE_SUB(CURDATE(), INTERVAL 3 DAY))
        LIMIT $threads"
    );
    // выход из функции, если актуальных записей не осталось
    if($stm->rowCount() == 0){
        echo "all mails had sended \n\r";
        return false;        
    }
    
    // массив для сохранения PID процессов
    $children = array();
    
    // перебериаем часть полученных записей из базы
    while($row = $stm->fetch(PDO::FETCH_ASSOC)) {
        $pid = pcntl_fork();
        if ($pid == -1) {
            die("Could not fork");
        } else if ($pid) {
            // сохраняем айди подпроцессов
            $children[] = $pid;
        } else {
            $email = $row['email'];
            //если проверка почты прошла успешно, то отправляем почту
            $checked = check_email($email);
            if($checked){
                send_email("company@mail.com", $email, "subscribe will expired soon!");
            } else {
            // если не успешно, то помечаем почту как valid = false (чтобы не брать их в новом запросе)
                $pdo = get_pdo();
                $pdo->prepare("UPDATE mailing_list SET valid = 0 WHERE email = '$email'")->execute();
                $pdo = null;
            }
            // exit child
            exit(0);
        }
    }
    
    // очищаем форк процессы с ожиданием завершения
    kill_forks($children);
    return true;
}

// заглушка проверки почты
function check_email(string $email):bool {    
    sleep(rand(1,10));  
    return true;
}

//заглушка для отправки почты
function send_email(string $from, string $to, string $content){ 
    global $start;
    sleep(rand(1,10));  
    $pdo = get_pdo();
    $pdo->prepare("UPDATE mailing_list SET last_sended = ".time()." WHERE email = '$to'")->execute();
    $pdo = null;     
    echo "time pass: ", (int)(microtime(true) - $start), "sec \r";
}

//очистка процессорных форков
function kill_forks(array $children){
    foreach ($children as $pid) {
        //ожидаем заврешения подпроцесса
        pcntl_waitpid($pid, $status);
        // посылаем сигнал на остановку
        posix_kill($pid, SIGKILL);
    }
}

// коннект с базой
function get_pdo(): PDO {
    $host = 'database';
    $db   = 'app';
    $user = 'root';
    $pass = 'secret';
    $charset = 'utf8';

    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $opt = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];
    $pdo = new PDO($dsn, $user, $pass, $opt);
    return $pdo;
}